# Welcome to my CST-120 Repository!


Here is a repository I made to share Discussion questions, code and other things school related.

## Discussion Questions
#### (Started repository Week 3)

## Week 3

- [Week 3 DQ 1](https://gitlab.com/sharepublic/school/-/blob/6fa6f3135c0e89c1c75f5c2f9c652268fc90b0b2/discussionQuestions/Wk3DQ1.md)

- [Week 3 DQ 2](https://gitlab.com/sharepublic/school/-/blob/c8a6e21f8c841a76a2a85f14d2d6519987b32b3d/discussionQuestions/Wk3DQ2.md)

## Week 4

- [Week 4 DQ 1](https://gitlab.com/sharepublic/school/-/blob/fa4cb8aca916c78c1960373fd29f269c519b42c1/discussionQuestions/Wk4DQ1.md)

- [Week 4 DQ 2](https://gitlab.com/sharepublic/school/-/blob/fa4cb8aca916c78c1960373fd29f269c519b42c1/discussionQuestions/Wk4DQ2.md)
