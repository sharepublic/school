# Discussion Questions

## Week 4

# DQ 2

| Aspect                    | Java                                         | JavaScript                                   |
|---------------------------|----------------------------------------------|----------------------------------------------|
| **Paradigm**              | Object-oriented (primarily)                  | Multi-paradigm (event-driven, functional)    |
| **Typing**                | Static typing                                | Dynamic typing                               |
| **Execution Environment** | Runs in a virtual machine or on a device     | Runs in a web browser or Node.js environment |
| **Use Cases**             | Web server, mobile, desktop applications     | Web pages, server-side (Node.js), databases  |
| **Syntax**                | C-like, with strict structure                | C-like, more flexible syntax                 |
| **Memory Management**     | Garbage collection                           | Garbage collection                           |
| **Concurrency**           | Threads                                      | Event loop, promises, async/await            |
| **File Extension**        | .java                                        | .js                                          |
| **Compilation**           | Compiled to bytecode and run on JVM          | Interpreted (or JIT compiled)                |
| **Standard Library**      | Extensive, including GUI, networking, etc.   | Focused on web-related functionality         |

| **Similarities**          | Java                                         | JavaScript                                   |
|---------------------------|----------------------------------------------|----------------------------------------------|
| **Syntax Inspiration**    | C-inspired syntax                            | C-inspired syntax                            |
| **Object-Oriented**       | Supports OOP concepts                        | Supports OOP concepts (prototype-based)      |
| **First-Class Functions** | Supported (since Java 8, with limitations)   | Supported                                    |
| **Platform-Independent**  | Write once, run anywhere                     | Code can run in any browser or Node.js       |
| **Community and Usage**   | Large community, widely used                 | Large community, widely used                 |
| **Error Handling**        | Exception handling                           | Error handling and try-catch                 |
| **Importing Modules**     | Import statement                             | Import/export statements                     |
| **Popular Usage**         | Enterprise applications, Android apps        | Web development, server-side scripting       |
| **Tools and IDEs**        | Eclipse, IntelliJ IDEA, NetBeans             | Visual Studio Code, Atom, Sublime Text       |
| **Version Management**    | Managed through JDK versions                 | Managed through ECMAScript standards         |





### Click [*Back to Main*](https://gitlab.com/sharepublic/school/-/tree/main) to go back to main page