# Discussion Questions

## Week 5

### DQ 2

# JSON vs XML Comparison

## Overview

| Feature             | JSON                                         | XML                                                      |
|---------------------|----------------------------------------------|----------------------------------------------------------|
| **Stands for**      | JavaScript Object Notation                   | Extensible Markup Language                               |
| **History**         | Released in 2001 by Douglas Crockford and Chip Morningstar | Released in 1998 by the XML Working Group              |
| **Format**          | Maplike structure with key-value pairs       | Tree structure with namespaces for different data categories |
| **Syntax**          | More compact, easier to read and write       | More verbose, uses entity references                     |
| **Parsing**         | Standard JavaScript function                 | XML parser                                               |
| **Schema Documentation** | Simple and more flexible               | Complex and less flexible                                |
| **Data Types**      | Numbers, objects, strings, Boolean arrays    | All JSON data types plus additional types like dates, images |
| **Ease of Use**     | Smaller file sizes, faster transmission      | More complex tag structure, bulky files                 |
| **Security**        | Safer                                        | Turn off DTD to mitigate risks                           |


## Conclusion:
- Looking at the comparison of code between Json and XML displaying names of guests the first thing I noticed is how much simpler the XML code looks vs the JSON code. The difference in format between is clear, JSON uses a maplike structure and XML uses a tree structure where the tree begins as at the root (parent). There is a lot to learn about the difference between these two data transport formats.