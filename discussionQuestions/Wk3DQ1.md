# Discussion Questions

## Week 3

# DQ 1
Here is my table for the history of CSS, I created it in HTML to practice. Learning about the history of CSS was an interesting read and I am glad I get to use it after it has advanced to where it is.
![DQ1 Table](../images/wk3dq1.png)

### Resources: 
- https://www.w3.org/Style/CSS20/history.html
- https://css-tricks.com/look-back-history-css/

### Click [*Back to Main*](https://gitlab.com/sharepublic/school/-/tree/main) to go back to main page