# Discussion Questions

## Week 7

### DQ 1


Here is the graph I made for DQ1

![DrawioBlockdiagram](media/wk7drawio.png)

The graph demonstrates how the HTTP protocol, client-side webpages with HTML forms, and server-side code are all used together to build powerful business applications as requested by the assignment.



