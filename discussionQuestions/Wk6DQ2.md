# Discussion Questions

## Week 6

### DQ 2

SASS, short for Syntactically Awesome Stylesheets,it is a preprocessor for CSS that makes it more powerful and easier to work with. It has several features that are not available in CSS yet like variables, nested rules, and inheritance to name a few. For example instead of repeating a color value throughout a style sheet, you can store it in a variable and just change that variable if you ever want to change it all. Here is an example I found while researching SASS.
![SASS_EXAMPLE](./media/Wk6DQ2.png)


### Conclusion
Having been the first time I have done any research on SASS, it appears to be a very useful tool when creating style sheets for a website. I will definitely be implementing it into my current stlye sheet.The first thing I noticed was nesting and how ill be able to nest everything for my nav bar under nav rather than having 10 different things that say nav-***.

#### Resources:
https://sass-lang.com/guide/