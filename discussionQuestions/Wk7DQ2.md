# Discussion Questions

## Week 7

### DQ 2

Discussion Question: 

In exploring dynamic web application frameworks that you might encounter in future courses, there are several notable options, each with distinct features and based on different programming languages.
In exploring dynamic web application frameworks that you might encounter in future courses, there are several notable options, each with distinct features and based on different programming languages.

#

### Response:
I went ahead and put a small description on several different frameworks for my own self benefit to refer back to later if I need. There is so much more to web development then appears on the surface and this is only scratching the surface. I look forward to future courses in this program.

**Ruby on Rails** is a full-stack web framework based on the Ruby language. It's known for its "convention over configuration" philosophy, which reduces the amount of manual setup for a project. Rails is a good fit for database-backed web applications and includes an extensive set of libraries for various tasks, with SQLite as the default database​.

**Laravel** is a PHP web framework acclaimed for its elegant syntax and robust features like built-in authentication and database migration tools. It facilitates the development process, making it a strong candidate for applications such as CRM systems and business process management tools. Laravel is flexible, allowing for use with various databases, and provides extensive documentation, making it beginner-friendly​​.

**React** is more of a JavaScript library maintained by Facebook  for building user interfaces then it is a framework. React focuses on the view layer, allowing you to create reusable UI components. It's known for its virtual DOM which optimizes rendering and enhances performance. React's ecosystem is vast and while it's great for the user interface​​.

**Django** is a high-level Python framework emphasizing rapid development and clean, pragmatic design. It follows the DRY principle and is very much a "batteries-included" framework. Django is used for developing complex data-driven websites and provides a built-in admin panel as well as security features​ 

**Vue.js**
 is a progressive JavaScript framework used for building user interfaces. It's designed to be incrementally adoptable, making it a good choice for projects that start small and gradually become more complex. Vue.js is appreciated for its simplicity, detailed documentation, and is a good option for those new to web framework technologies​

Each of these frameworks has a distinct set of tools and features that cater to different aspects of web application development, and the choice depends on the specific requirements of the project and the developer's familiarity with the underlying language and concepts.

## Resources
https://five.co/blog/the-5-best-web-frameworks/
