# Discussion Questions

## Week 6

### DQ 1
Hello Class,
I basically just skimmed through each of the different documentations for each of the frameworks I researched.
### Bootstrap
 is a popular front-end framework known for its extensive library of pre styled components and responsive design templates making it popular choice for developers aiming for fast website development.

### Tailwind CSS
Tailwind is all about giving you the reins with its utility-first vibe, letting you piece together your own unique design without boxing you in. It’s a bit more hands-on and can get complex, but it’s perfect for when you want your site to stand out. 

### Foundation
Foundation is more similar to bootstrap but keeps things more streamlined and code-friendly, focusing on being lightweight and giving you just what you need to get your site responsive and fast.

### Conclusion

While bootstrap seems the fastest way to get your site up and going, Tailwind and Foundation are easier to tailor to what you want and focus more on keeping load times zippy.

### Resources
#### Tailwind
https://tailwindcss.com/docs/installation


