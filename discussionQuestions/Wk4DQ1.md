# Discussion Questions

## Week 4

# DQ 1
## Table for History of JavaScript


JavaScript has had Several changes since '95. This was interesting to see all the changes over the years and when they were implemented.

| Year | What Happened?                           | What's New?                                    | Why It Matters                                   |
|------|------------------------------------------|------------------------------------------------|--------------------------------------------------|
| 1995 | JavaScript was born at Netscape.         | It was created to make websites interactive.   | It started the journey of dynamic web pages.     |
| 1997 | ECMAScript (the official standard) came. | Set the rules for JavaScript to ensure consistency. | Made sure JavaScript worked the same everywhere. |
| 1999 | ECMAScript 3 added more features.        | Brought in regular expressions and better error handling. | Made JavaScript smarter and less error-prone.    |
| 2009 | ECMAScript 5 introduced major updates.   | Added "strict mode" and improved array handling. | Made scripts safer and arrays more versatile.     |
| 2015 | ECMAScript 2015 (or ES6) revamped JS.    | Introduced classes, arrow functions, and promises. | Made code cleaner and introduced modern concepts. |
| 2016 | ECMAScript 2016 kept it simple.          | Added Array.includes and exponentiation (**).   | Small but handy additions for developers.        |
| 2017 | ECMAScript 2017 made async easy.         | Brought in async/await for better async calls.  | Simplified writing asynchronous code.            |
| 2018 | ECMAScript 2018 expanded features.       | Added rest/spread properties and async iteration. | Made dealing with objects/arrays easier.         |
| 2019 | ECMAScript 2019 polished things up.      | Flattening arrays and new string methods added. | Enhanced array and string operations.            |
| 2020 | ECMAScript 2020 went big with BigInt.    | Introduced BigInt and dynamic imports.          | Supported larger numbers and modular JS code.    |
| 2021 | ECMAScript 2021 brought more syntax.     | Added logical assignment operators and more.    | Further streamlined coding syntax and operations.|

#### Resources:
- https://developer.mozilla.org/en-US/docs/Web/JavaScript





### Click [*Back to Main*](https://gitlab.com/sharepublic/school/-/tree/main) to go back to main page