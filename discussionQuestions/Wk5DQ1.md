# Discussion Questions

## Week 5

### DQ 1
There are many advantages and disadvantages when it comes to using JavaScript frameworks. A huge advantage is that creating your own has quite a learning curve and using a pre-made framework that you know works and has been tested to work can save a lot of time. A disadvantage of using JavaScript frameworks is you could use your own custom code designed to use only what you want it to do without a bunch of extra things you do not need or use hindering performance. All in all the main advantage to using JavaScript frameworks versus using your own code would be time and if JavaScript frameworks get the job done without hindering performance it seems more logical to me to use them if applicable.

